package com.awesome.uid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UidApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(UidApplication.class, args);
    }

    @Autowired
    @Qualifier("CachedUidGenerator")
    private UidGenerator uidGenerator;

    @Override
    public void run(String... args) {
        System.out.println(uidGenerator.getUID());
    }
}
