package com.awesome.uid;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class UidApplicationTests {

    @Autowired
    @Qualifier("CachedUidGenerator")
    private UidGenerator uidGenerator;

    @Test
    void contextLoads() {
        System.out.println(uidGenerator.getUID());
    }

}
